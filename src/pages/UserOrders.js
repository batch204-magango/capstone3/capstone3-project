import { useState, useEffect } from 'react';
import { Accordion } from 'react-bootstrap';

export default function UserOrders(){

	const token = localStorage.getItem("token")
	const [orders, setOrders] = useState([])
	const [userDetails, setUserDetails] = useState([])

	useEffect(()=> {


		fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			const userDetails = data.map(userDetail => {
				return (
				<Accordion.Item eventKey={userDetail._id}>
				  <Accordion.Header>{userDetail.email}
				  </Accordion.Header>
				  <Accordion.Body className="accordion">
				  {userDetail.orders.map(order => {
				  	return (
				  		<div>
					  		<div>Purchased on: {order.purchasedOn.substring(0,10)} {order.purchasedOn.substring(11,19)}</div>
					  		{order.products.map(product => {
						    	return (
						    		<ul>
						    			<li><span className="text-primary">{product.name}</span> - <span className="text-danger">Quantity: {product.quantity}</span></li>
						    		</ul>
						    	)
						    })}
						    <div>Total: <span className="text-danger">₱{order.totalAmount}</span></div>
						    <hr/>
				  		</div>
				  		)
				  	})}
				  	
				  </Accordion.Body>
				</Accordion.Item>
				)
			})

			setUserDetails(userDetails)
		})
	}, [])

	return (
		<Accordion>
	      {userDetails}
	    </Accordion>
	)
}
