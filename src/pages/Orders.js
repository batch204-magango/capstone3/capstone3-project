import { useState, useEffect } from 'react';
import { Accordion } from 'react-bootstrap';

export default function Orders(){

	const token = localStorage.getItem("token")
	const [orders, setOrders] = useState([])

	useEffect(()=> {


		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data.orders.length !== 0){
				let counter = 0
				const orders = data.orders.map(order => {
					counter ++
					return (

						<Accordion.Item eventKey={order._id}>
						  <Accordion.Header>Order No. {counter} - Purchased on: {order.purchasedOn.substring(0,10)} {order.purchasedOn.substring(11,19)}</Accordion.Header>
						  <Accordion.Body className="accordion">
						  	<div>Items:</div>
						    {order.products.map(product => {
						    	return (
						    		<ul>
						    			<li><span className="text-primary">{product.name}</span> - <span className="text-danger">Quantity: {product.quantity}</span></li>
						    		</ul>
						    	)
						    })}
						    <div>Total: <span className="text-danger">₱{order.totalAmount}</span></div>
						  </Accordion.Body>
						</Accordion.Item>
					)
				})

				setOrders(orders)
			}
			else{
				const orders = <h1>No Orders Yet</h1>
				setOrders(orders)
			}
			
		})
	}, [])

	return (
		<Accordion>
	      {orders}
	    </Accordion>
	)
}

