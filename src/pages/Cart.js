import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Cart(){

	const [ productsArr, setProductsArr] = useState([])
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [cart, setCart] = useState([])
	const [qty, setQty] = useState(1)
	const [totalAmount, setTotalAmount] = useState(0)

	const token = localStorage.getItem("token")

	useEffect(()=> {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}
	}, [])

	useEffect(() => {
		const products = cart.map(product => {
			return(
				<tr className="custom-bg custom-border-color" key={product._id}>
					<td className="custom-border-color">{product.name}</td>
					<td className="custom-border-color">{product.price}</td>
					<td className="custom-border-color"><Button className="custom-btn" size="sm" onClick={() => minusOne(product.quantity, product.productId)}>-</Button>
					<input
						className="number-input"
						type="number"
						value={product.quantity}
						onChange={e => {
						   	if(e.target.value <= 0){
						   		if(e.target.value === ""){
						   			alert("This field should not be empty")
						   		}
						   		else{
						   			alert("Quantity cannot be lower than 1.")
						   		}
						   		e.preventDefault();
						    }
						   	else{
						   		qtyInput(product.productId, e.target.value)
						   	}
						}}
					/><Button className="custom-btn" size="sm" onClick={() => plusOne(product.quantity, product.productId)}>+</Button></td>	
					<td className="custom-border-color">{product.subtotal}</td>
					<td className="custom-border-color"><Button className="custom-remove" variant="primary" size="sm" onClick={() => removeItem(product.productId)}>Remove</Button></td>
				</tr>
			)
		})

		setProductsArr(products)

	}, [cart])

	const minusOne = (qty, productId) => {
		qty = parseInt(qty)
		if(qty>1){
			qty -= 1
		qtyInput(productId, qty)
		}	
		else{
			alert("Quantity cannot be lower than 1.")
		}
	}

	const plusOne = (qty, productId) => {
		qty = parseInt(qty)
		qty += 1
		qtyInput(productId, qty)
	}

	const qtyInput = (productId, value) => {


		let tempCart = [...cart]

		for(let i = 0; i < tempCart.length; i++){
			if(tempCart[i].productId === productId){		
				tempCart[i].quantity = parseFloat(value)
				tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
			}
		}

		setCart(tempCart)

		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}


	const removeItem = (productId) => {

		let index = ""
		let tempCart = [...cart]
		
		for(let i = 0; i<tempCart.length; i++){
			if(tempCart[i].productId === productId){
				index = i
			}
		}

		tempCart.splice(index, 1)

		setCart(tempCart)


		localStorage.setItem('cart', JSON.stringify(tempCart))	



	}

	useEffect(()=> {

		let currentTotalAmount = 0
		let tempCart = [...cart]
		
		for(let i = 0; i<tempCart.length; i++){
			currentTotalAmount += parseInt(tempCart[i].subtotal)
		}

		setTotalAmount(currentTotalAmount)
		
	}, [cart])

	

	function checkout(){

			fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
				method: "POST",
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					"products": cart,
			    	"totalAmount":totalAmount
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
			})

			localStorage.removeItem('cart')

			alert("Order placed! Thank you!")
		}

	if(localStorage.getItem('cart')){
		if(JSON.parse(localStorage.cart).length !== 0){
			return (
				<>
					<h2>Your Shopping Cart</h2>
					<Table bordered hover responsive>
						<thead className="bg-gray text-white">
							<tr className="custom-border-color">
								<th>Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{productsArr}
							<tr className="custom-bg custom-border-color">
								<td colSpan="3" className="custom-border-color"><Button className="warning-primary custom-btn" to={`/cart`} onClick={() => checkout()}>Checkout</Button></td>
								<td colSpan="2" className="custom-border-color">Total: {totalAmount}</td>
							</tr>
						</tbody>
					</Table>
				</>
			)
		}

		return(<h2>Your cart is empty!<Link  style={{textDecoration: 'none'}} to={`/products`}> Start Shopping</Link></h2>)
	}
	else{
		return(<h2>Your cart is empty!<Link  style={{textDecoration: 'none'}} to={`/products`}> Start Shopping</Link></h2>)
	}
	
}




{/*<h1 className=" mt-5 mb-3 text-center">Your Shopping Cart</h1>
			<div className="table-responsive">
				<Table striped bordered hover className="">
			      <thead>
			        <tr>
			          <th>Name</th>
			          <th>Price</th>
			          <th>Quantity</th>
			          <th colSpan={2}>Subtotal</th>
			        </tr>
			      </thead>

			      <tbody>
			        <tr>
			          <td>{Name}</td>
			          <td>&#8369; {Price}</td>
			          <td> 
			          		<Button variant="primary" type="submit" size="sm">
			          			    -
			          		</Button>

			          		&nbsp;&nbsp;<input type="number" placeholder={quantity}/>&nbsp;&nbsp;

			          		<Button variant="primary" type="submit" size="sm">
			          		 	+
			          		</Button>
			     
			          </td>
			          <td>{total}</td>
			          <td><Button variant="danger" size="sm" onClick={removeItem}>Remove</Button></td>
			        </tr>

			        <tr>
			          <td colSpan={3}><Button variant="success" size="lg" className="">Checkout</Button></td>
			          <th colSpan={2}> Total: {total}</th>
			        </tr>
			      </tbody>
			</Table>


			</div>

		</>
	);
}
*/}