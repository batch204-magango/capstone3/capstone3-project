//page for registration.
import { useState, useEffect, useContext } from 'react';
import '../App.css';
import { Form, Button, Card } from 'react-bootstrap';
//111111111111111
import UserContext from '../UserContext';
import {Link, Redirect} from 'react-router-dom';
//111111111111111
export default function Register(props) {
	// console.log(props)

	//this is where we will save what the customer would type/imput in the form
	//from registration schema
	const [firstName, setFirstName] = useState(""); //you will see @gmail.com inside the place holder.
	const [lastName, setLastName] = useState(""); //you will see @gmail.com inside the place holder.
	const [email, setEmail] = useState(""); //you will see @gmail.com inside the place holder.
	const [mobileNo, setMobileNo] = useState(""); //you will see @gmail.com inside the place holder.
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false); //false is the default for isActive, we will use this for our button, activates and deactivates the button when the values are correct, the second button is disabled or grayed out
	
	const { user } = useContext(UserContext);
	//onChange={e => setEmail(e.target.value)}, onChange is an event we will use this to get the value of the email that a user types, or capture on change of the value as the user types that value.
	/*
		To properly change and save input values, we must implement two-way binding, We need to capture whatever the user types in the input as they are typing, meaning we need the input's .value value.
		To get the .value, we capture the event (in this case, onChange). The target of the onChange event is the input, meaning we can get the .value
	*/
	/*2 Syntax:
    useEffect(()=> {
        code to be executed/console logged.
    }, [state(s) to monitor or getter and setter])
    */
	useEffect(()=> {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "") && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, password1, password2]) //since we are monitoring the changes of these 3.

	function registerUser(e) { 
		e.preventDefault() //prevent the form behaviour. so that the form does not submit
		// setEmail("")
		// setPassword1("")
		// setPassword2("")
		// alert("Thank you for registering!")

		//always check POSTMAN to see if the same format when sending a request.
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Duplicate email exist")
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName:firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password:password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						alert("Successfully registered")

						props.history.push("/login")
					}else{
						alert("Something went wrong")
					}
				})
			}
			// console.log(data)
		})
	}

	//we used 'onSubmit' 'event' to bind the function registerUser() to the form upon submission
	return (
		(user.id !== null) 
		?
		<Redirect to="/" /> //redirect to home if email is not null.
		:
		<container>
		<div class="justify-content-center p-4 p-3 mb-2  d-flex align-items-stretch">
		<Card className="mb-2 p-5 ">
		<h1 className="text-center">Register</h1>

		<Form onSubmit={e => registerUser(e)}> 
			{/*start---------------------------*/}
			<Form.Group ControlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)} //onchange event, the value of the target is now our 'new' 'useState' of 'setEmail' value, this is what the user types to the email
					required
				/>
			</Form.Group>

			<Form.Group ControlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter last name"
					value={lastName}
					onChange={e => setLastName(e.target.value)} //onchange event, the value of the target is now our 'new' 'useState' of 'setEmail' value, this is what the user types to the email
					required
				/>
			</Form.Group>
			{/*end---------------------------*/}





			<Form.Group ControlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)} //onchange event, the value of the target is now our 'new' 'useState' of 'setEmail' value, this is what the user types to the email
					required
				/>
				<Form.Text className="text-muted">	
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			{/*start---------------------------*/}
			<Form.Group ControlId="mobileNo">
				<Form.Label>Mobile Numer</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter mobile number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)} //onchange event, the value of the target is now our 'new' 'useState' of 'setEmail' value, this is what the user types to the email
					required
				/>
			</Form.Group>
			{/*end---------------------------*/}

			<Form.Group ControlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password" //if you change this to "text" you will see the password instead of bullets/disc
					placeholder="Enter pasword"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group ControlId="pasword2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
				<div className="text-center">
					
			{
				isActive 
					? 
				<Button 
					className=" button mt-3"
					variant="primary" 
					type="submit" 
					id="submitBtn">
					Submit
				</Button> 
					 :
				<Button 
					className="mt-3"
					variant="primary" 
					type="submit" 
					id="submitBtn"
					disabled>
					Submit
				</Button> 
			}
				</div>

			<p className="text-right mt-3">
			          Already have an account? <Link to="/login">Click here</Link> to log in.
			</p>


		</Form>

</Card>


		</div>
		</container>
		
	)
}