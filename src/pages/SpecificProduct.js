import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import UserContext from '../UserContext';

export default function SpecificProduct({match}) {

	const { user, setUser } = useContext(UserContext);
	
	const [description, setDescription] = useState("");
	const [id, setId] = useState("")
	const [name, setName] = useState("");
	const [price, setPrice] = useState(0);
	const [qty, setQty] = useState(1)
	const [total, setTotal] = useState(0)
	const [cart, setCart] = useState([])
	const productId = match.params.productId;

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setId(productId)
		})
	}, [])

	useEffect(()=> {
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
		}
	}, [])

	useEffect(()=> {

		let tempTotal = 0

		cart.forEach((item)=> {
			tempTotal += item.subtotal
		})

		setTotal(tempTotal)
	}, [cart])

	const addToCart = () => {
		let alreadyInCart = false
		let productIndex
		let cart = []

		if(localStorage.getItem('cart')){
			cart = JSON.parse(localStorage.getItem('cart'))
		}

		for(let i = 0; i < cart.length; i++){
			if(cart[i].productId === id){
				alreadyInCart = true
				productIndex = i
			}
		}

		if(alreadyInCart){
			cart[productIndex].quantity = parseInt(cart[productIndex].quantity) + parseInt(qty)
			cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
		}

		else{
			cart.push({
				'productId' : id,
				'name': name,
				'price': price,
				'quantity': qty,
				'subtotal': price * qty
			})		
		}

		localStorage.setItem('cart', JSON.stringify(cart))

		if(qty === 1){
			alert("1 item added to cart.")
		}
		else{
			alert(`${qty} items added to cart.`)
		}
		
	}

	const minusOne = (qty) => {
		qty = parseInt(qty)
		if(qty>1){
			setQty(qty-=1)
		}	
		else{
			alert("Quantity cannot be lower than 1.")
		}
	}

	const plusOne = (qty) => {
		qty = parseInt(qty)
		setQty(qty+=1)
	}

	return(
		<Container className="mt-5">
		<Row className="my-3 justify-content-center">
		<Col xs={11} md={10} lg={6}>
			<Card >
				<Card.Body className="text-center custom-bg">

					<Card.Title className="text-primary mb-3">{name}</Card.Title>

					<Card.Text className="text-muted">{description}</Card.Text>

					<Card.Text className="text-danger">₱{price}</Card.Text>
					

					<Card.Subtitle className="mb-3">Quantity:</Card.Subtitle>
					<div><Button className="btn-warning me-1 custom-btn" size="md" onClick={() => minusOne(qty)}>-</Button>
					<input 
						className="number-input"
						type="number"
						defaultValue={1}
						value={qty}
						onChange={e => {
						   	if(e.target.value <= 0){
						   		if(e.target.value === ""){
						   			alert("This field should not be empty")
						   			e.target.value = 1
						   		}
						   		else{
						   			alert("Quantity cannot be lower than 1.")
						   			e.target.value = 1
						   		}
						   		e.preventDefault();
						    }
						    else{
						    	setQty(e.target.value)
						    }
						}}


					/>
					<Button className="btn-warning ms-1 custom-btn" size="md" onClick={() => plusOne(qty)}>+</Button></div>

					{user.id !== null 
						?
						<>
							<Button className="me-2 mt-3 custom-btn" variant="primary" onClick={addToCart} >Add to Cart</Button>
							 <Link className="text-warning8 custom-btn ms-2 mt-3" to={`/cart`}>Go to Cart</Link>
						</>
						:
						<Link className="btn custom-btn" to={`/login`}>Log in to Add to Cart</Link>
					}

				</Card.Body>
			</Card>
		</Col>
		</Row>
		</Container>
	)
}



















// import { useState, useEffect } from 'react';
// import { Container, Card,  } from 'react-bootstrap';
// import { Link } from 'react-router-dom';

// export default function SpecificProduct({match}) {

// 	const [name, setName] = useState("");
// 	const [description, setDescription] = useState("");
// 	const [price, setPrice] = useState(0);
// 	//match.params holds the ID of our course in the courseId property
// 	const productId = match.params.productId;
// 	// console.log(courseId)
// 	const [quantity, setQuantity] = useState(1);
// 	const [handleDecrement, handleIncrement] = useState("");

// 	useEffect(() => {

// 		const handleDecrement = () =>{
// 			setQuantity(prevCount => prevCount - 1);
// 		}
// 			const handleIncrement = () =>{
// 			setQuantity(prevCount => prevCount + 1);
// 		}

// 		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
// 		.then(res => res.json())
// 		.then(data => {
// 			setName(data.name)
// 			setDescription(data.description)
// 			setPrice(data.price)
// 			console.log(data)
// 		})
// 	}, [])



// 	return(
// 		<Container className="mt-5 p-5">
// 			<Card>
// 				<div class="bg-secondary text-white text-center pb-0 card-header"><h4 className= "text-center"><Card.Title>{name}</Card.Title></h4>
// 				</div>

// 					<div class="card-body">
// 						<p class="card-text">	<Card.Text>{description}</Card.Text>
// 						</p>
// 						<h4>Price:</h4>
// 						<span class="text-warning">			<Card.Text>{price}</Card.Text>
// 						{/*"&#8369;319.00"*/}
// 						</span>
// 						<h4>Quantity:</h4>



// 						<div className= "col-md-3 mt-3">
// 						<div className="input-group">
// 						<button type="button"onclick={handleDecrement}  className="input-group-text">-</button>
// 						<div className="form-control text-center">{quantity}</div>
// 						{/*<input type="text" className="form-control text-center"/>*/}
// 						<button type="button"  onclick={handleIncrement} className="input-group-text">+</button>
							
// 						</div>
							

// 						</div>
// 						<div className="col-md-3 mt-3">
// 						<button type="button" className="btn btn-primary w-100">Add to Cart</button>
							
// 						</div>








						


							


// 						  </div>



// 				{/*<Card.Body className="text-center text-strong bg-secondary text-white pb-0 card-header">
// 						<Card.Subtitle></Card.Subtitle>
// 					<Card.Title>{name}</Card.Title>
// 					<Card.Subtitle>Description:</Card.Subtitle>
// 					<Card.Text>{description}</Card.Text>
// 					<Card.Subtitle>Price:</Card.Subtitle>
// 					<Card.Text>{price}</Card.Text>
				
					

				


// 				</Card.Body>*/}
// 			</Card>
// 		</Container>
// 	)
// }


