import { useState, useEffect, useContext} from 'react';
import { Form, Button, Card} from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import  UserContext  from '../UserContext';



export default function Login(props){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

	const { user, setUser} = useContext(UserContext);

	


	useEffect(() => {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)
		if((email !== '' && password !== '' ) ) {
			setIsActive(true)
			}else{
			setIsActive(false)
			
		}
	}, [email, password])

	const retrieveUserDetails = (token) => {

		// console.log('hello')
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			
			headers: {
				Authorization:`Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log('hello')
			//set our user state to inclue the user's id and isAdmin values
			setUser({
				id:data._id,
				isAdmin: data.isAdmin
			})
				// console.log(user.id)
		})
	}

	function loginUser(e){
		e.preventDefault()//prevent default form behavior, so that the form does not submit

		/*activity s55  API Integration with Fetch
			Create a fetch request inside this function to allow users to login.
			log in the console the response
		*/
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
// console.log(data)
			if(typeof data.access !== "undefined"){
				//save JWT to localstorage
				localStorage.setItem("token", data.access)
				//call the retrieveUserDetails function and pass the JWT to it
				retrieveUserDetails(data.access)

				alert("Successfully logged in.")
				props.history.push("/")
			}else{
				alert("Login failed. Please try again")
				setEmail("");
				setPassword("");

			}
			// console.log(data);
		});


		//localStorage.setItem allows
	// 	localStorage.setItem('email', email)

	// 	setUser({
	// 		email: email
	// 	})


	// 	setEmail("")
	// 	setPassword("")
	
	// 	alert('Thank you for logging in')
	 }

	return(

	
		(user.id !== null) ?
			<Redirect to="/"/>
		  	:
		
		  	<container className="mt-5 p-0">
		 
		  	<div class="login">
		  		<div class="justify-content-center p-4 p-3 mb-2  d-flex align-items-stretch">

		<Card className="mb-2 p-5 ">
			<Form onSubmit ={e => loginUser(e)}>
		<h1 className="text-center">Log In</h1>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value ={email}
					onChange ={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
				
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					value ={password}
					onChange ={e => setPassword(e.target.value)}
					required
				/>
				
			</Form.Group>

			<div className="text-center">
					{isActive ?
				<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<Button className="mt-3" variant="primary"id="submitBtn" disabled>
				Login
			</Button>
		}
			</div>

		

	<p className="text-right mt-3">
			          Don't have an account yet? <Link to="/register">Click here</Link> to register.
			</p>

		
			


			
		</Form>
		</Card>
	
			


		  	</div>
		  	</div>
		  	
		  	</container>
		
	)


		
}