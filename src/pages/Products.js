import { useEffect, useState, useContext } from 'react';
// import courseData from '../data/courseData';
import ProductsCard from '../components/ProductsCard';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';


export default function Products(){

	const [productData,setProductsData] = useState([])

	const { user } = useContext(UserContext)
		console.log(user)

	//Check to see if the mock data we captured
	// console.log(courseData);
	// console.log(courseData[0]);

	//Props
		//is a shorthand for property since components are considered as object in ReactJS
		//Props is a way to pass data from a parent component to a child component.
		//It is a synonymous to the function parameter
		//This is reffered to as "props drilling".

		const fetchData = () => {

			fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then (data => {
			console.log(data)
		setProductsData(data)
	
		})
		}
		useEffect(() => {

			// console.log(process.env.REACT_APP_API_URL)
		fetchData()
	}, [])

		const product = productData.map(products => {

				if(products.isActive){
		return(
 		<ProductsCard productProp={products} key={products._id}/>
				)

				}else{
					return null
				}


			

		})


	return(
		(user.isAdmin) ?
		<AdminView productProp={productData} fetchData={fetchData}/>
		:

		<>
			<h1>Products</h1>
			{product}
		  


		 </>


		)
}