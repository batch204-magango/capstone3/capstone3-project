import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function Home() {

    const data = {
        title: "Helmet Collection",
        content: "Find something memorable.",
        destination: "/products",
        label: "Click here to view all products"
    }

    return (
        <>
            <Banner dataProp= {data} />
            <Highlights />
        </>
    )
}
// import React from "react";
// import { Link, Redirect } from 'react-router-dom';
// export default function Home() {

// // const Home = () => {
//     return (

//         <div className="hero">
//         <div class="card bg-dark text-black border-0">
//         {/*<img src="" class="card-img" alt="Background" height = "800px" />*/}
//         <div class="card-img-overlay">
//         <div className="container">
//             <h5 class="card-title display-3 fw-bolder mb-0 text-center pt-5">New Season Arrivals</h5>
//             <p class="card-text pt-4">
                
//             </p>
//             <p className="text-center mt-3">
//                       Add to Cart?  Login First <Link to="/register">Click here</Link> to register.
//             </p>


//         </div>
            
//         </div>  
//          </div>
            
//         </div>







//         )
// }
    