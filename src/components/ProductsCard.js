import { useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// Destructuring is done in the parameter to retrieve the courseProp.
export default function ProductsCard({productProp}) {

    // console.log(props.courseProp);
    // console.log(courseProp);

    const {_id,name, description, price} = productProp;

//         return (
        
//         <Card className="mb-2 custom-bg">
//             <Card.Body>
//                 <Card.Title className="text-primary">{name}</Card.Title>
//                 <Card.Text className="text-muted">{description}</Card.Text>
//                 <Card.Text className="text-danger">₱{price}</Card.Text>
//                 <Link className="btn custom-btn" to={`/products/${_id}`}>Details</Link>

//             </Card.Body>
//         </Card>
//     )
// }


    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax: 
        // const [getter, setter] = useState(initialGetterValue)

        //when a component mounts (loads for the first time), any associated states will undergo a state change from null to the fiven initial/default state

        //eg. count below goes from null to 0, since 0 is our initial state

    // const [count, setCount] = useState(0);
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.
    // const [seats, setSeats] = useState(10)
    // 1. Create a "seats" state hook on the "CourseCard" component to represent the available seats. 

    // console.log(useState(0));

    // Function that keeps track of the enrollees for a course
    // By default JavaScript is synchronous, as it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
    // The setter function for useStates are asynchronous, allowing it to execute seperately from other codes in the program.
    // The "setCount" function is being executed while the "console.log" is already being completed resulting in the console to be behind by one count.

    // 2. Modify the "enroll" function where a seat will be taken and the number of enrollees will go up on clicking the "Enroll" button
    // function enroll () {
    // // 4. Create a condition that will check if the "seats" is "0" alert the user of no more courses.
    //     // if (seats > 0) {
    //     // setCount(count + 1);
    //     // console.log('Enrollees: ' + count)
    //         setCount(count + 1);
    //         // console.log('Enrollees: ' + 1);
    //         setSeats(seats - 1)
            // console.log('Seats: ' + seats)
    //     } else {
    //         alert('No more seats available')
    //     };
    // }
// }

//     //useEffect makes any given code block happen when a state  changes AND when a component first mounts(such as on initial page load)
//     useEffect(() => {
//         if(seats ===0){
//             alert('No more seats available')
//         }
//     // console.log("State changed")
// }, [count,seats])

    //syntax
    /*
    useEffect(()) => {
    code to be executed
    }, [state(S) to monitor]


    */


    // 3. Include the number of seats to be printed out in the card.
    return (
        <Card className="mb-2 ">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                
                <Card.Text>PhP {price}</Card.Text>

               
               
{
                 
}
            <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}

//    /*
// <Card.Text>Enrollees: {count}</Card.Text>
//                 <Card.Text>Seats: {seats}</Card.Text>
//                 <Button variant="primary" onClick={enroll}>Enroll</Button>
//                 */