import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminView(props){

	//destructure the coursesProp and the fetchData function from Courses.js
	const { productProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)

	//states for handling modal visibility
	const [showEdit, setShowEdit] = useState(false)
	//
	const [showAdd, setShowAdd] = useState(false)
	const [isActive, setIsActive] = useState(false)

	const token = localStorage.getItem("token")

	//Functions to handle opening and closing modals




	const openEdit = (productId) => {
		// console.log(courseId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
		setShowEdit(true)
	}

	const closeEdit = () => {
			setProductId("")
			setName("")
			setDescription("")
			setPrice(0)
		setShowEdit(false)
	}

	const editProducts = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully updated")
				//close the modal and set all states back to default values
				closeEdit()
				fetchData()
				//we call fetchData here to updated the we recieve from the database
				//calling fetchdata update our coursesProp, which the useEffect below is monitoring
				//since the courseProp updates, the useEffect runs again, which re-renders our table with the updated data

			}else{
				alert("Something went wrong")

			}

		})
	}

	const openAdd = () => {
		setName("");
		setDescription("");
		setPrice();

		setShowAdd(true)
		// fetch()
	}

	const closeAdd = () => {
		setProductId("");
		setName("");
		setDescription("");
		setPrice(0);

		setShowAdd(false)
	}


	////
	const addProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/add-product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully Added")
	
				closeAdd()
				fetchData()
		

			}else{
				alert("Something went wrong")

			}

		});
	}


	/////

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				let bool
				isActive ? bool = "disabled": bool = "enabled"
				alert(`Product successfully ${bool}`)
				fetchData()
			} else{
				alert("Something went wrong")

			}
		})

	}

	useEffect(() => {
		//map through the coursesProp to generate table contents
		const products = productProp.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
							{/*Dynamically render course availability*/}
							{product.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							//dynamically render which button show depending on product availability
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		//set the CoursesArr state with the results of our mapping so that it can be used in the return statement
		setProductsArr(products)

	}, [productProp])
	
	return(
		<>
			<h2 className="text-center mt-5 mb-3 " >Admin Dashboard</h2>
			<div className="text-center">
				<Button className=" btn-primary  mb-4 p-0  btn-warning" variant="primary" size="sg" onClick={() => openAdd()}>Add Product</Button>
		
		
				{/*<Button className="btn-primary  mb-4 p-0 btn-warning" variant="primary" size="sg" onClick={() => orders/all()}>Show user Order</Button>*/}
				<Button className="btn-primary  mb-4 p-0  btn-warning" variant="primary" size="sg" to={`/products/all`}>Show user Order
					
				</Button>
			</div>



			{/*Product info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the coursesProp*/}
					{productsArr}
				</tbody>
			</Table>

			{/*Edit Product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit ={e => editProducts(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="editProductName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
		


						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button  variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>



		{/*Add Product Modal*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit ={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="addProductName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
		


						<Form.Group controlId="addproductDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="addproductsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button  variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>





		</>
	)
}


