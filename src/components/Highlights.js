import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){
    return(


        <Row className="my-3">


            <Col xs={12} md={4}>


                <Card className="cardHighlight p-3 mb-2  ">

                    <Card.Body>

                        <Card.Title className="text-center">
                       
                            <h2>DIY Tactical helmets No1 and No2 template for EVA foam</h2>
                        </Card.Title>

                        <Card.Text className="text-center">
                      DIGITAL ITEM (printable PDF file): This is a 2in1 printable EVA foam template for 2 different full head tactical helmets - simple and advance build.
                        </Card.Text>
                        <h5 class="text-warning p-3">
                            
                            "&#8369;"
                            319.00
                        </h5>
                    </Card.Body>
                    <a class="btn btn-primary  btn-block" href="/products/636e5dc85792ea4ec8bc8ee0">Details</a>
                </Card>
                </Col>



                <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 mb-2">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h2>LARP Leather Viking Helmet PATTERN - Viking Set</h2>
                        </Card.Title>
                        <Card.Text className="text-center">
                            Ever wanted to walk round as a badass an stimmt be protected? This is one way to do so. A relatively simple project for someone with experience. The most tricky part the the glueing of the helmet base.
                        </Card.Text>
                          <h5 class="text-warning">
                            
                            "&#8369;"
                            1,118.00
                        </h5>
                    </Card.Body>

                    <a class="btn btn-primary btn-block" href="/products/636e5dfc5792ea4ec8bc8ee3">Details</a>
                </Card>
                </Col>
                <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 mb-2">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h2>Knight helmet foam pattern bundle</h2>
                        </Card.Title>
                        <Card.Text className="text-center">
                         This is a digital template for you to print out on paper, cut out and use to assemble your own wearable helmet using EVA foam sheets. These are not 3D files, they are PDF templates already converted for you to use with foam, no need to unfold 3d models!
                        </Card.Text>
                          <h5 class="text-warning">
                            
                            "&#8369;"
                           425.53.00
                        </h5>
                    </Card.Body>

                    <a class="btn btn-primary btn-block" href="/products/636e5e195792ea4ec8bc8ee6">Details</a>
                </Card>
            </Col>

               <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 mt-4">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h2>DIY Barbarian No1 helmet template for EVA foam</h2>
                        </Card.Title>
                        <Card.Text className="text-center">
                         DIGITAL ITEM (printable PDF file): This is a printable EVA foam template for a Barbarian inspired helmet with horns. Very simple and easy to make, ideal if you are a foam-smith, fan, collector, or planning to cosplay.
                        </Card.Text>
                          <h5 class="text-warning">
                            
                            "&#8369;"
                           298.00
                        </h5>
                    </Card.Body>
                    <a class="btn btn-primary btn-block" href="/products/636e5ec95792ea4ec8bc8ef7">Details</a>
                </Card>
            </Col>

               <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 mt-4">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h2>Anduin Wrynn metal lion helmet， 1:6 Scale Anduin Wrynn cosplay helme</h2>
                        </Card.Title>
                        <Card.Text className="text-center">
                        Anduin Wrynn metal lion helmet， 1:6 Scale Anduin Wrynn cosplay helmet, WOW Anduin Wrynn, World of Warcraft collection Ornament
                        </Card.Text>
                          <h5 class="text-warning">
                            
                            "&#8369;"
                           2,409.00
                        </h5>
                    </Card.Body>
                    <a class="btn btn-primary btn-block" href="/products/636e5ef45792ea4ec8bc8efa">Details</a>
                </Card>
            </Col>

               <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 mt-4">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h2>PrimeArt Handmade 18'' Inch Nautical Anchor Engineer Diving / Divers</h2>
                        </Card.Title>
                        <Card.Text className="text-center">
                         This is a handcrafted item · Divers Helmet is characterized with a big glass lens · Divers Helmet is available in Black/Yellow/Silver/Copper/Antique Finish · This is an excellent adorable art work of high quality · Use it as home or office decorative, desktop Table decorative
                        </Card.Text>
                          <h5 class="text-warning">
                            
                            "&#8369;"
                            12,522.00
                        </h5>
                    </Card.Body>
                     <a class="btn btn-primary btn-block" href="/products/636e5f265792ea4ec8bc8efd">Details</a>
                </Card>
            </Col>

               <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 mt-5">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h2>Cyberpunk mask - Cyber mask - Samurai helmet - Tactical helmet Cosplay</h2>
                        </Card.Title >
                        <Card.Text className="text-center">
                        If you choose to add braids, please let me know your choice of color and number. If you need to change the blue color, please let me know.
                        </Card.Text>
                          <h5 class="text-warning">
                            
                            "&#8369;"
                            7,828.00
                        </h5>
                    </Card.Body>
                     <a class="btn btn-primary btn-block" href="/products/636e5f625792ea4ec8bc8f00">Details</a>
                </Card>
            </Col>

               <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 mt-5">
                    <Card.Body>
                        <Card.Title className="text-center">
                            <h2>Leather Norse Helm Foam PATTERN / TEMPLATEe</h2>
                        </Card.Title>
                        <Card.Text className="text-center">
                       If you are a warrior looking to keep your head covered, look no further than the Leather Norse Helm. Create your own version of a classic Viking helmet that can be used for a variety and styles of characters.
                        </Card.Text>
                          <h5 class="text-warning">
                            
                            "&#8369;"
                            301.00
                        </h5>
                    </Card.Body>
                           <a class="btn btn-primary btn-block" href="/products/636e5f895792ea4ec8bc8f03">Details</a>
                </Card>
            </Col>

        </Row>
    )
}